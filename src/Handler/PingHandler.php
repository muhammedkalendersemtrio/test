<?php

namespace App\Handler;

use App\Entity\Test;
use App\Message\Ping;
use App\Repository\TestRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class PingHandler implements MessageHandlerInterface
{
    private TestRepository $testRepository;

    /**
     * @param TestRepository $testRepository
     */
    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    public function __invoke(Ping $message)
    {
        $time = new \DateTime();

        $a = new Test();
        $a->setTest('SQS-' . $time->format('c'));
        $a->setName('Name-' .$message->getData());

        $this->testRepository->save($a, true);

        return json_encode($a);
    }
}