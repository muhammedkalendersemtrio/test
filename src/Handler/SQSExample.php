<?php

namespace App\Handler;

use App\Entity\Test;
use App\Repository\TestRepository;
use Bref\Context\Context;
use Bref\Event\Sqs\SqsEvent;
use Bref\Event\Sqs\SqsHandler;

class SQSExample extends SqsHandler
{
    private TestRepository $testRepository;

    /**
     * @param TestRepository $testRepository
     */
    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    public function handleSqs(SqsEvent $event, Context $context): void
    {
        foreach ($event->getRecords() as $record) {
            // We can retrieve the message body of each record via `->getBody()`
            $body = $record->getBody();

            $a = new Test();
            $a->setTest('SQS' . $body);
            $a->setName('Name');

            $this->testRepository->save($a, true);

            // do something
        }
    }
}