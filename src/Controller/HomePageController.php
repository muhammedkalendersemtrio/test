<?php

namespace App\Controller;

use App\Entity\Test;
use App\Repository\TestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    private TestRepository $testRepository;

    /**
     * @param TestRepository $testRepository
     */
    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    #[Route('/')]
    public function getPage()
    {
        dd($this);
        $time = new \DateTime();

        $a = new Test();
        $a->setName('WEB-'.$time->format('c'));
        $a->setTest('TEST');

        $this->testRepository->save($a, true);

        return $this->render('home.html.twig', [
            'datetext' => $time->format('H:i:s \O\n Y-m-d'),
            'tests' => $this->testRepository->findAll()
        ]);
    }
}