<?php

namespace App\Lambda;

use App\Entity\Test;
use App\Repository\TestRepository;

class TestLambda
{
    private readonly TestRepository $testRepository;

    /**
     * @param TestRepository $testRepository
     */
    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    public function __invoke($data = '')
    {
        $time = new \DateTime();

        $test = new Test();
        $test->setName('Function-' . $time->format('c'));
        $test->setTest('AA' . implode(",", $data));

        $this->testRepository->save($test, true);

        return implode(",", $data) . $test->getName();
    }
}